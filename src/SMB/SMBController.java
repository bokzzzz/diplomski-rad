package SMB;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.Base64;
import java.util.LinkedList;
import java.util.List;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;

public class SMBController {
	private static final String USER_NAME = "kali";
	private static final String PASSWORD = "kali";
	private static final String NETWORK_FOLDER = "smb://192.168.1.6/SMBFiles";
	private static NtlmPasswordAuthentication auth;
	static {
		auth = new NtlmPasswordAuthentication(USER_NAME + ":" + PASSWORD);
	}

	public static List<SmbFile> getFiles(String dir) throws SmbException {
		SmbFile rootDir;
		try {
			rootDir = new SmbFile(NETWORK_FOLDER + dir, auth);
			List<SmbFile> files = new LinkedList<SmbFile>(Arrays.asList(rootDir.listFiles()));
			return files;
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}

	}

	public static String downloadFile(String pathFile) throws IOException {
		SmbFile file;
		file = new SmbFile(NETWORK_FOLDER + pathFile, auth);
		if (file.isFile()) {
			InputStream is = file.getInputStream();
			ByteArrayOutputStream result = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int length;
			while ((length = is.read(buffer)) != -1) {
				result.write(buffer, 0, length);
			}
			is.close();
			String resultStr= Base64.getEncoder().encodeToString(result.toByteArray());
			result.close();
			return resultStr;
		} else
			return null;
	}
}
