package convert;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.net.ftp.FTPFile;

import com.google.gson.Gson;

import Model.FileModel;

public class FtpFileConverter {
	public static String convert(List<FTPFile> files)  {
		List<FileModel> filesModel = new LinkedList<FileModel>();
		for(FTPFile file:files)
			filesModel.add(new FileModel(file.getName(),(file.isFile()? "File":"Dir" )));
		return new Gson().toJson(filesModel);
	}
}
