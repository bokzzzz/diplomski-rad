package convert;

import java.util.LinkedList;
import java.util.List;

import com.google.gson.Gson;

import Model.FileModel;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;

public class SmbFileConverter {
	public static String convert(List<SmbFile> files) throws SmbException {
		List<FileModel> filesModel = new LinkedList<FileModel>();
		for(SmbFile file:files) {
			filesModel.add(new FileModel(file.getName(),(file.isFile()? "File":"Dir" )));
		}
		return new Gson().toJson(filesModel);
	}
}
