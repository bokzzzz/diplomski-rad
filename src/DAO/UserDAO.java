package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import Model.User;
import connection.ConnectionPool;

public class UserDAO {
	public User getUserByUsernameAndPassword(String username,String password) {
		String sql = "select * from user where username=? and password=?";
		User user = null;
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, username);
			statement.setString(2, password);
			ResultSet resultSet = statement.executeQuery();
			if(resultSet.next()) {
				user=new User();
				user.setId(resultSet.getInt("id"));
				user.setUsername(resultSet.getString("username"));
				user.setPassword(resultSet.getString("password"));			
			}
			resultSet.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return user;
	}
	public boolean isSMBActivated(String username) {
		String sql = "select isSMBActivated from user where username=? ";
		boolean result=false;
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, username);
			ResultSet resultSet = statement.executeQuery();
			if(resultSet.next()) {
				result=resultSet.getInt("isSMBActivated") == 1 ? true:false;
			}
			resultSet.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return result;
	}
	public boolean isFTPActivated(String username) {
		String sql = "select isFTPActivated from user where username=? ";
		boolean result=false;
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, username);
			ResultSet resultSet = statement.executeQuery();
			if(resultSet.next()) {
				result=resultSet.getInt("isFTPActivated") == 1 ? true:false;
			}
			resultSet.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return result;
	}
	public boolean isHTTPActivated(String username) {
		String sql = "select isHTTPActivated from user where username=? ";
		boolean result=false;
		Connection connection = null;
		try {
			connection = ConnectionPool.getConnectionPool().checkOut();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, username);
			ResultSet resultSet = statement.executeQuery();
			if(resultSet.next()) {
				result=resultSet.getInt("isHTTPActivated") == 1 ? true:false;
			}
			resultSet.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			ConnectionPool.getConnectionPool().checkIn(connection);
		}
		
		return result;
	}
	public List<String> getActivatedServices(String username){
		List<String> services= new LinkedList<String>();
		if(isSMBActivated(username)) services.add("SMB");
		if(isFTPActivated(username)) services.add("FTP");
		if(isHTTPActivated(username)) services.add("HTTP");
		return services;
	}
	
}
