package detect;

public class CheckParam {
	public static boolean isSafeParameter(String param) {
		MySqlInjection mysqli=new MySqlInjection();
		boolean result=XSSAttack.isXSSSafe(param) && mysqli.isSqlInjectionSafe(param);
		return result;
	}
}
