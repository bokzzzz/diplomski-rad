package filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class PathFilter implements Filter {

    public PathFilter() {
    }

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		HttpSession session = req.getSession();
		String defaultPage=req.getContextPath()+"/";
		String loginURI = req.getContextPath() + "/login.html";
		String filesURI = req.getContextPath() + "/files.html";
		String maliciousURI = req.getContextPath() + "/malicious.html";
		String forbiddenURI = req.getContextPath() + "/forbidden.html";
		String resources=req.getContextPath() + "/resources";
		String loginServlet=req.getContextPath()+"/login";
		String fileServlet=req.getContextPath()+"/userFiles";
		String downloadServlet=req.getContextPath()+"/download";
		String httpServlet=req.getContextPath()+"/http";
		String accessServlet=req.getContextPath()+"/access";
		String logoutServlet=req.getContextPath()+"/logout";
		String httpAppServlet=req.getContextPath()+"/http/login";
		String httpFileServlet=req.getContextPath()+"/http/file";
		String favicon="/favicon.ico";
		System.out.println("=================================================");
		System.out.println(req.getRequestURI());
		if(req.getRequestURI().startsWith(resources)) {
			chain.doFilter(req, res);
		}
		else if(req.getRequestURI().equals(defaultPage) && session.getAttribute("username") != null) {
			res.sendRedirect(filesURI);
		}
		else if(req.getRequestURI().equals(loginURI) && session.getAttribute("username") != null) {
			res.sendRedirect(filesURI);
		}else if(req.getRequestURI().equals(filesURI) && session.getAttribute("username") == null) {
			res.sendRedirect(loginURI);
		}
		else if(req.getRequestURI().equals(downloadServlet) && session.getAttribute("username") == null) {
			res.sendRedirect(loginURI);
		}
		else if(req.getRequestURI().equals(httpServlet) && session.getAttribute("username") == null) {
			res.sendRedirect(loginURI);
		}
		else if(req.getRequestURI().equals(httpAppServlet) && session.getAttribute("username") == null) {
			res.sendRedirect(loginURI);
		}
		else if(req.getRequestURI().equals(accessServlet) && session.getAttribute("username") == null) {
			res.sendRedirect(loginURI);
		}
		else if(req.getRequestURI().equals(logoutServlet) && session.getAttribute("username") == null) {
			res.sendRedirect(loginURI);
		}
		else if(req.getRequestURI().equals(httpFileServlet) && session.getAttribute("username") == null) {
			res.sendRedirect(loginURI);
		}
		else if(req.getRequestURI().equals(loginURI) || req.getRequestURI().equals(loginServlet )
			|| req.getRequestURI().equals(fileServlet) || req.getRequestURI().equals(favicon) || req.getRequestURI().equals(filesURI)
			|| req.getRequestURI().equals(defaultPage) || req.getRequestURI().equals(downloadServlet) 
			|| req.getRequestURI().equals(httpServlet) || req.getRequestURI().equals(accessServlet)
			|| req.getRequestURI().equals(forbiddenURI) || req.getRequestURI().equals(logoutServlet) 
			|| req.getRequestURI().equals(maliciousURI) || req.getRequestURI().equals(httpAppServlet)
			|| req.getRequestURI().equals(httpFileServlet)) {
			chain.doFilter(req, res);
		}
		else {
			res.sendRedirect(loginURI);
		}
	}


}
