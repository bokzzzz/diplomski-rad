package ftp;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Base64;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;


public class FTPController {
	private static final String SERVER = "192.168.1.6";
	private static final int PORT = 21;
	private static final String USER_NAME = "kali";
	private static final String PASSWORD = "kali";

	public static List<FTPFile> getFiles(String path) throws IOException {
		FTPClient ftpClient = new FTPClient();
		ftpClient.connect(SERVER, PORT);
		ftpClient.login(USER_NAME, PASSWORD);
		FTPFile[] files = ftpClient.listFiles(path);
		ftpClient.logout();
		ftpClient.disconnect();
		return new LinkedList<FTPFile>(Arrays.asList(files));
	}

	public static String downloadFile(String pathFile) throws IOException {
		FTPClient ftpClient = new FTPClient();
		ftpClient.connect(SERVER, PORT);
		ftpClient.login(USER_NAME, PASSWORD);
		InputStream is = ftpClient.retrieveFileStream(pathFile);
		if (is != null) {
			ByteArrayOutputStream result = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int length;
			while ((length = is.read(buffer)) != -1) {
				result.write(buffer, 0, length);
			}
			String returnResult=Base64.getEncoder().encodeToString(result.toByteArray());
			is.close();
			result.close();
			ftpClient.logout();
			ftpClient.disconnect();			
			return returnResult;
		} else
			return null;
	}
}
