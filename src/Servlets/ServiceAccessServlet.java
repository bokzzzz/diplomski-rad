package Servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import DAO.UserDAO;


@WebServlet("/access")
public class ServiceAccessServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDAO userDAO=new UserDAO();
	private Gson gson=new Gson();
    public ServiceAccessServlet() {
        super();
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getSession().getAttribute("username") != null) {
			response.getWriter().print(gson.toJson(userDAO.getActivatedServices((String)request.getSession().getAttribute("username"))));
		}else response.setStatus(400);
		response.getWriter().close();
	}

}
