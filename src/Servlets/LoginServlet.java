package Servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import detect.CheckParam;
import login.LoginController;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public LoginServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		if (CheckParam.isSafeParameter(username) && CheckParam.isSafeParameter(password)) {
			boolean isOK = LoginController.isCredentialsOK(username, password);
			if (isOK) {
				request.getSession().setAttribute("username", username);
			}
			response.getWriter().print(String.valueOf(isOK));
			response.getWriter().close();
		}else response.setStatus(400);
	}

}
