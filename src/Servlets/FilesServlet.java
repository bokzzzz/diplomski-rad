package Servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.UserDAO;
import SMB.SMBController;
import convert.FtpFileConverter;
import convert.SmbFileConverter;
import detect.CheckParam;
import ftp.FTPController;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;

@WebServlet("/userFiles")
public class FilesServlet extends HttpServlet {
	private static final long serialVersionUID = -4479469688057583741L;
	private static final String FTP = "FTP";
	private static final String SMB = "SMB";
	private UserDAO userDao = new UserDAO();

	public FilesServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("Velicina: " + Runtime.getRuntime().freeMemory() / 1048576);
		System.out.println(
				"Koristim: " + ((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1048576));
		String path = request.getParameter("path");
		String fileSharingType = request.getParameter("fileSharingType");
		int statusCode = 200;
		if (CheckParam.isSafeParameter(path) && CheckParam.isSafeParameter(fileSharingType)) {
			if (request.getSession().getAttribute("username") != null && path != null && !path.isEmpty()
					&& fileSharingType != null && !fileSharingType.isEmpty()) {
				if (fileSharingType.equals(SMB)) {
					if (userDao.isSMBActivated((String) request.getSession().getAttribute("username"))) {
						try {
							List<SmbFile> files = SMBController
									.getFiles("/" + request.getSession().getAttribute("username") + path);
							if (files != null) {
								String responseJson = SmbFileConverter.convert(files);

								response.getWriter().println(responseJson);

							} else {
								statusCode = 503;
							}
						} catch (SmbException e) {
							if (e.getNtStatus() == SmbException.NT_STATUS_UNSUCCESSFUL) {
								statusCode = 503;
							} else if (e.getNtStatus() == SmbException.NT_STATUS_OBJECT_PATH_NOT_FOUND) {
								statusCode = 400;
							}
						}
					} else {
						statusCode = 403;
					}
				} else if (fileSharingType.equals(FTP)) {
					if (userDao.isFTPActivated((String) request.getSession().getAttribute("username"))) {
						try {
							String responseJson = FtpFileConverter.convert(
									FTPController.getFiles(request.getSession().getAttribute("username") + path));
							response.getWriter().println(responseJson);
						} catch (IOException e) {
							statusCode = 400;
						}
					} else {
						statusCode = 403;
					}
				} else {
					statusCode = 400;
				}

			} else {
				statusCode = 400;
			}

		} else {
			statusCode = 400;
		}
		response.setStatus(statusCode);
		response.getWriter().close();
	}

}
