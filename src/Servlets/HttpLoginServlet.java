package Servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.CookieManager;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import detect.CheckParam;


@WebServlet("/http/login")
public class HttpLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String SOCKET = "http://192.168.1.6:8080/http/login";
	static final String COOKIES_HEADER = "Set-Cookie";
	
    public HttpLoginServlet() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		if (CheckParam.isSafeParameter(username) && CheckParam.isSafeParameter(password)) {
			URL url = new URL(SOCKET);
			URLConnection con = url.openConnection();
			HttpURLConnection http = (HttpURLConnection)con;
			http.setRequestMethod("POST"); 
			http.setDoOutput(true);
			Map<String,String> arguments = new HashMap<>();
			arguments.put("username", username);
			arguments.put("password", password); 
			StringJoiner sj = new StringJoiner("&");
			for(Map.Entry<String,String> entry : arguments.entrySet()) {
				sj.add(URLEncoder.encode(entry.getKey(), "UTF-8") + "=" + URLEncoder.encode(entry.getValue(), "UTF-8"));
			}
			    
			byte[] out = sj.toString().getBytes(StandardCharsets.UTF_8);
			int length = out.length;
			http.setFixedLengthStreamingMode(length);
			http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
			http.connect();
			
			try(OutputStream os = http.getOutputStream()) {
			    os.write(out);
			}
			CookieManager msCookieManager = new CookieManager();

			Map<String, List<String>> headerFields = http.getHeaderFields();
			List<String> cookiesHeader = headerFields.get(COOKIES_HEADER);

			if (cookiesHeader != null) {
			    for (String cookie : cookiesHeader) {
			        msCookieManager.getCookieStore().add(null,HttpCookie.parse(cookie).get(0));
			    }    
			    for(HttpCookie cookie:msCookieManager.getCookieStore().getCookies()) {
			    	System.out.println(cookie.getValue());
			    	System.out.println(cookie.getSecure());
			    	System.out.println(cookie.getName());
			    	System.out.println(cookie);
			    }
			    System.out.println("setovano");
			    request.getSession().setAttribute("CookieHttp", msCookieManager);
			}
			try(InputStream in = http.getInputStream()){
				BufferedReader inBuf = new BufferedReader(new InputStreamReader(in));
				StringBuilder responseHTML = new StringBuilder();
				String inputLine;
				while ((inputLine = inBuf.readLine()) != null)
					responseHTML.append(inputLine);
				in.close();
				response.getWriter().print(responseHTML.toString());
				response.getWriter().close();
			}
		}else response.sendRedirect("/malicious.html");
	}

}
