package Servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.UserDAO;

@WebServlet("/http")
public class HTTPServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String SOCKET = "http://192.168.1.6:8080/";
	private UserDAO userDao =new UserDAO();
	public HTTPServlet() {
		super();
	}

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (userDao.isHTTPActivated((String) request.getSession().getAttribute("username"))) {
			URL website = new URL(SOCKET);
			URLConnection connection = website.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			StringBuilder responseHTML = new StringBuilder();
			String inputLine;
			while ((inputLine = in.readLine()) != null)
				responseHTML.append(inputLine);
			in.close();
			response.getWriter().print(responseHTML.toString());
			response.getWriter().close();

		}else response.sendRedirect(request.getContextPath()+"/forbidden.html");
	}

}
