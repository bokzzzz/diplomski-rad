package Servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.UserDAO;
import SMB.SMBController;
import detect.CheckParam;
import ftp.FTPController;

@WebServlet("/download")
public class DownloadFileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String FTP = "FTP";
	private static final String SMB = "SMB";
	private UserDAO userDAO = new UserDAO();

	public DownloadFileServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String path = request.getParameter("path");
		String fileSharingType = request.getParameter("fileSharingType");
		int statusCode = 200;
		if (CheckParam.isSafeParameter(path) && CheckParam.isSafeParameter(fileSharingType)) {
			if (request.getSession().getAttribute("username") != null && path != null && !path.isEmpty()
					&& fileSharingType != null && !fileSharingType.isEmpty()) {
				if (fileSharingType.equals(SMB)) {
					if (userDAO.isSMBActivated((String) request.getSession().getAttribute("username"))) {
						try {
							String result = SMBController
									.downloadFile("/" + request.getSession().getAttribute("username") + path);
							if (result != null)
								response.getWriter().write(result);
							else
								statusCode = 400;
						} catch (IOException e) {
							statusCode = 400;
						}
					} else
						statusCode = 403;
				} else if (fileSharingType.equals(FTP)) {
					if (userDAO.isFTPActivated((String) request.getSession().getAttribute("username"))) {
						try {
							String result = FTPController
									.downloadFile(request.getSession().getAttribute("username") + path);
							if (result != null)
								response.getWriter().write(result);
							else
								statusCode = 400;
						} catch (IOException e) {
							statusCode = 400;
						}
					} else
						statusCode = 403;
				} else
					statusCode = 400;

			} else
				statusCode = 400;
			
		}else statusCode=400;
		response.setStatus(statusCode);
		response.getWriter().close();
	}
}
