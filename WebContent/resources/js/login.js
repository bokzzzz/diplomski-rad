

$( document ).ready(function() {
	
	$('#loginButton').click(function(){ 
		$('#loginPart').append($('<div>').attr('id','loader').attr('class','loader'));
		var username=$('#username').val();
		var password=$('#password').val();
		$('#resultLogin').empty();
		if(username === '' || password==='')
			alert('Please fill the fields!');
		else{
			$.ajax({
  				url: "/login",
  				type: "POST",
				data: {username:username,password:password},
  				success: function(resp){
					console.log(resp+resp.length);
    				if(resp === 'true'){
						//sessionStorage.setItem("user",username);
						$(location).attr('href', 'files.html');
						}
						else {
							$('#loader').remove();
							$('#resultLogin').append($('<p>').text('Wrong username or password!'));
						}
 				 },
				 error:function(XMLHttpRequest, textStatus, errorThrown){
					if(XMLHttpRequest.status == 400){
						window.location = "/malicious.html";
					}
			}
			});
		}
 });
});