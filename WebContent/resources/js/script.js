var currentDirPath=[];

$( document ).ready(function() {
	setChoice();
});

function setDir(path,dirName){
	var filesDiv=$('#files');
	$.ajax({
  		url: "/userFiles",
  		type: "POST",
		data:{path:path,fileSharingType:sessionStorage.getItem('service')},
  		success: function(html){
    		var files=JSON.parse(html);
			$('#files').empty();
			setNavigationPath(dirName);
			for(file of files){
				var itemDiv=$('<div>').attr('class','itemDiv');
				var fileElem;
				var img;
				if(file.type === 'File'){
					fileElem=$('<a>').attr('href','').click(downloadFile).appendTo(itemDiv);
					img=$('<img>').	attr('src','/resources/images/fileIcon.png').appendTo(fileElem);
					fileElem=$('<a>').attr('href','').click(downloadFile).appendTo(itemDiv);					 
				}
				else{
					fileElem=$('<a>').attr('href','').click(onClickDir).appendTo(itemDiv);
					img=$('<img>').	attr('src','/resources/images/dirIcon.png').appendTo(fileElem);
					if(file.name.endsWith('/')) file.name=file.name.slice(0,-1);
				}
				var name=$('<p>').text(file.name).appendTo(fileElem);
				
				
				$('#files').append(itemDiv);
			}
  		},
		error:function(XMLHttpRequest, textStatus, errorThrown){
			if(XMLHttpRequest.status == 400){
				window.location = "/malicious.html";
			}else if(XMLHttpRequest.status == 503){
				$('#path').empty();
				var serviceText=$('<p>').text('Service is not working.');
				$('#path').append(serviceText);
			}
			else if(XMLHttpRequest.status == 403){
				window.location = "/forbidden.html";
			}
		}
});
	
}

function onClickDir(event){
	event.preventDefault();
	var dirName=$(this).find('p').text();
	currentDirPath.push(dirName);
	setDir(calcPath(),dirName);	
}
function calcPath(){
	var path='/';
	for(str of currentDirPath){
		path+=str;
		path+='/';
	}
	return path;
}

function setNavigationPath(nextDir){
	var divPath=$('#path');
	var aDir=$("<a>");
	$(aDir).attr('id',nextDir).attr('href','').text(nextDir).click(onClickNavPath).appendTo(divPath);
	var slash=$('<p>').text('/').appendTo(divPath);
	$('#path').append(aDir);
}
function onClickNavPath(event){
	event.preventDefault();
	var dirName=$(this).text();
	if(dirName === 'root'){
		currentDirPath=[];
		$('#path a').empty();
		$('#path p').empty();
		setDir('/','root');
	}
	else{
		var popDir;
		do{
			popDir=currentDirPath.pop();
			$('#path a:last').remove();
			$('#path p:last').remove();
		}while(popDir !== dirName);
		currentDirPath.push(popDir);	
		setDir(calcPath(),dirName);
	}	
}
function setChoice(){
	if(sessionStorage.getItem("service"))
		{
			setRootDir();
		}
		else{
			$.ajax({
  		url: "/access",
  		type: "GET",
  		success: function(resp){
			var services=JSON.parse(resp);
			var percentWidth=73/services.length;
			if(services.length > 0){
				var choiceSer=$('<p>').attr('class','choiceP').text('Choice service:');
				$('#serviceChoiceDiv').append(choiceSer);
			}
			for(service of services){
				if(service === 'SMB'){
					var smbImg=$('<img>').attr('class','serviceImg').attr('src','resources/images/smb.jpg').click(function(){
						return onClickService('SMB');
					});
					$(smbImg).width(percentWidth+'vw');
					$('#serviceChoiceDiv').append(smbImg);
				}
				else if(service === 'FTP'){
					var ftpImg=$('<img>').attr('class','serviceImg').attr('src','resources/images/ftp.jpg').click(function(){
						return onClickService('FTP');
					});
					$(ftpImg).width(percentWidth+'vw');
					$('#serviceChoiceDiv').append(ftpImg);
				}else if(service === 'HTTP'){
					var httpImg=$('<img>').attr('class','serviceImg').attr('src','resources/images/http.jpg').click(function(){
						console.log("http")
						window.location = "/http";
					});
					$(httpImg).width(percentWidth+'vw');
					$('#serviceChoiceDiv').append(httpImg);
				}
			}
  		},
		error:function(XMLHttpRequest, textStatus, errorThrown){
			if(XMLHttpRequest.status == 400){
				var serviceText=$('<p>').text('Bad request from client.');
				$('#serviceChoiceDiv').append(serviceText);
			}
		}
	});
			
		
			
		}
}
function setRootDir(){
	var divPath=$("<div>").attr('class','pathDiv');
	$('#path').append($('<img>').attr('class','pathImg').attr('src','resources/images/pathImage.png'));
	$(divPath).attr('id',  'divPath');
	setDir('/','root');
}
function onClickService(service){
	sessionStorage.setItem('service',service);
	$('#serviceChoiceDiv').empty();
	setRootDir();
}
function downloadFile(event){
		event.preventDefault();
		var fileName=$(this).text();
		var fullPath=calcPath()+fileName;
		$.ajax({
  		url: "/download",
  		type: "POST",
		data:{path:fullPath,fileSharingType:sessionStorage.getItem('service')},
  		success: function(resp){
			var element = document.createElement('a');
			$(element).attr('href','data:;base64,' + resp);
			$(element).attr('download',fileName);
			$(element).removeAttr("style").hide();
			$("body").append(element);
			element.click();
  			$(element).remove();
  		},
		error:function(XMLHttpRequest, textStatus, errorThrown){
			if(XMLHttpRequest.status == 400){
				//$('#path').empty();
				//$('#files').empty();
				//var serviceText=$('<p>').text('Bad request from client. Can not download file.');
				//$('#path').append(serviceText);
				window.location = "/malicious.html";
			}
			else if (XMLHttpRequest.status == 403){
				window.location = "/forbidden.html";
			}
		}
});
}
function logout(){
	sessionStorage.clear();
	$.ajax({
  		url: "/logout",
  		type: "GET",
  		success: function(){
			sessionStorage.clear();
			window.location = "/login.html"
  		}});
}